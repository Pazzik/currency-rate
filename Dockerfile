FROM ruby:2.6.3-alpine
RUN apk update \
&& apk upgrade \
&& apk add --update --no-cache build-base \
curl-dev git postgresql-dev yaml-dev zlib-dev nodejs tzdata

RUN gem install bundler

WORKDIR /app

COPY Gemfile* ./

RUN bundle install

COPY . /app
#RUN groupadd --gid 1000 app && useradd --uid 1000 --gid 1000 app --shell /bin/bash