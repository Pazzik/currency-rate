window.onload = function() {
    const ctx = document.getElementById('currency-chart').getContext('2d');
    const chart_data = $('#currency-chart').data('payload');
    const min = Math.min(...chart_data.buy);
    const max = Math.max(...chart_data.sell);
    const gap = max - min;
    const myChart = new Chart(ctx, {
        type: 'line',
        data: {
            datasets: [{
                label: 'buy',
                data: chart_data.buy,
                borderColor: 'rgb(255,91,98)',
                fill: false,
                borderWidth: 5
            },{
                label: 'sell',
                data: chart_data.sell,
                borderColor: 'rgb(94,95,255)',
                fill: false,
                borderWidth: 5
            }],
            labels: chart_data.time
        },
        options: {
            title: {
                display: true,
                text: chart_data.currency_pair
            },
            scales: {
                yAxes: [{
                    ticks: {
                        min: min - 0.5 * gap,
                        max: max + 0.5 * gap
                    }
                }]
            }
        }
    });
};
