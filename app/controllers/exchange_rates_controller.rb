# frozen_string_literal: true

class ExchangeRatesController < ApplicationController
  helper_method :form_data

  def index
    exchange_rates = ExchangeRate.where('created_at > ?', Time.current - 1.day)
                                 .where(from_name: form_data[:from])
                                 .where(to_name: form_data[:to])
    @chart_data = chart_data(exchange_rates).to_json
  end

  private

  def form_data
    @form_data ||= {
      from: params[:from] || 'USD',
      to: params[:to] || 'EUR',
      from_names: ExchangeRate.distinct.pluck(:from_name),
      to_names: ExchangeRate.distinct.pluck(:to_name)
    }
  end

  def chart_data(exchange_rates)
    {
      buy: exchange_rates.map(&:buy),
      sell: exchange_rates.map(&:sell),
      time: exchange_rates.map { |er| er.created_at.strftime('%H:%M') },
      currency_pair: "#{form_data[:from]}/#{form_data[:to]}"
    }
  end
end
