# frozen_string_literal:true

require 'open-uri'

class ExchangeRatesParser
  BASE_URL = 'https://www.tinkoff.ru'

  def parse
    exchange_rates.select do |er|
      er['category'] == 'DepositPayments'
    end
  end

  private

  def exchange_rates_url
    "#{BASE_URL}/api/v1/currency_rates"
  end

  def raw_data
    URI.parse(exchange_rates_url).read
  end

  def json_data
    JSON.parse(raw_data)
  end

  def exchange_rates
    json_data.dig('payload', 'rates').to_a
  end
end
