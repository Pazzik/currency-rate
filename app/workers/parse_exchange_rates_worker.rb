# frozen_string_literal: true

class ParseExchangeRatesWorker
  include Sidekiq::Worker

  def perform
    current_time = Time.current
    bulk_data = ExchangeRatesParser.new.parse.map do |er|
      {
        from_code: er.dig('fromCurrency', 'code'),
        from_name: er.dig('fromCurrency', 'name'),
        to_code: er.dig('toCurrency', 'code'),
        to_name: er.dig('toCurrency', 'name'),
        buy: er['buy'],
        sell: er['sell'],
        created_at: current_time,
        updated_at: current_time
      }
    end

    ExchangeRate.upsert_all(bulk_data)
  end
end
