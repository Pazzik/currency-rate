# frozen_string_literal: true

class CreateExchangeRates < ActiveRecord::Migration[6.0]
  def change
    create_table :exchange_rates do |t|
      t.integer :from_code
      t.string  :from_name
      t.integer :to_code
      t.string  :to_name
      t.numeric :buy,  precision: 15, scale: 6
      t.numeric :sell, precision: 15, scale: 6

      t.timestamps
    end
  end
end
