# frozen_string_literal: true

FactoryBot.define do
  factory :exchange_rate do
    from_code { rand(1000) }
    sequence(:from_name) { |n| "cur#{n}" }
    to_code { rand(1000) }
    sequence(:to_name) { |n| "cur#{n + 1}" }
    buy { rand * 1000 }
    sell { rand * 1000 }
  end
end
