# frozen_string_literal: true

RSpec.describe ExchangeRatesParser, :vcr do
  it 'returns correct array' do
    result = described_class.new.parse
    expect(result.class).to eq(Array)
    expect(result[0]).to include(
      'category' => 'DepositPayments',
      'fromCurrency' =>
          { 'code' => 840, 'name' => 'USD', 'strCode' => '840' },
      'toCurrency' =>
          { 'code' => 643, 'name' => 'RUB', 'strCode' => '643' },
      'buy' => 63.25,
      'sell' => 65.8
    )
  end
end
