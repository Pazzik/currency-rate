# frozen_string_literal: true

RSpec.describe ParseExchangeRatesWorker do
  it 'creates records' do
    allow_any_instance_of(ExchangeRatesParser).to receive(:parse).and_return(
      [
        { 'category' => 'DepositPayments',
          'fromCurrency' =>
             { 'code' => 840, 'name' => 'USD', 'strCode' => '840' },
          'toCurrency' =>
             { 'code' => 643, 'name' => 'RUB', 'strCode' => '643' },
          'buy' => 63.25, 'sell' => 65.8 }
      ]
    )
    described_class.new.perform
    expect(ExchangeRate.first).to have_attributes(
      from_code: 840,
      from_name: 'USD',
      to_code: 643,
      to_name: 'RUB',
      buy: 63.25,
      sell: 65.8
    )
  end
end
